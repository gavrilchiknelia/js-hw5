'use strict';
function checkString(arg, arg2) {
    while (arg === '' || !isNaN(arg)){
        arg = prompt(`Enter your ${arg2} name: `, '');
    }
    return  arg;
}

function CreateNewUser() {
    let obj = {
        firstName: prompt('Введите имя', ''),
        lastName: prompt('Введите фамилию', ''),
        birthday: prompt('введите дату рождения в формате ДД.ММ.ГГГГ', ''),
        setFirstName: function(value) {
            Object.defineProperty(this, 'firstName', {
                writable: true
            });
            this.firstName = value;
            Object.defineProperty(this, 'firstName', {
                writable: false
            });
        },
        setLastName: function(value) {
            Object.defineProperty(this, 'lastName', {
                writable: true
            });
            this.lastName = value;
            Object.defineProperty(this, 'lastName', {
                writable: false
            });
        },
        getLogin : function () {
            return this.firstName.charAt(0) + this.lastName;
        },

        getAge: function () {
            let theBirthday = this.birthday.split('.');
            let birth = new Date(theBirthday[2], theBirthday[1], theBirthday[0]);
            let now = new Date();
            let age = now.getFullYear() - birth.getFullYear();
            let m = now.getMonth() - birth.getMonth();
            if (m < 0 || (m === 0 && now.getDate() < birth.getDate())) {
                age--;
            }
            return age;
        },
        getPassword: function(){
            let theBirthday = this.birthday.split('.');
            let birth = new Date(theBirthday[2], theBirthday[1], theBirthday[0]);
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + birth.getFullYear();
        }
    };

    Object.defineProperties(obj, {
        'firstName': {
            writable: false
        },
        'lastName': {
            writable: false
        }
    });

    return obj;

}

let newUser = new CreateNewUser();
console.log(newUser.getAge());
console.log(newUser.getPassword());
console.log(newUser);
console.log(newUser.getLogin());
newUser.setFirstName('Lili');
console.log(newUser);
newUser.firstName = 'Bob';
console.log(newUser);